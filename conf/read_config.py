import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

if "MODULE_SERVER" in os.environ:
    __MODULE_SERVER__ = os.getenv("MODULE_SERVER")
else:
    __MODULE_SERVER__ = config['DEFAULT']['MODULE_SERVER']

if "DATA_PATH" in os.environ:
    __DATA_PATH__ = os.getenv("DATA_PATH")
else:
    __DATA_PATH__ = config['LOFTOOL']['DATA_PATH']

if "PORT" in os.environ:
    __PORT__ = os.getenv("PORT")
else:
    __PORT__ = config['LOFTOOL']['LOFTOOL_ADAPTER_PORT']

__SCORE_FILE__ = config['LOFTOOL']['SCORE_FILE']

col_names = [

]