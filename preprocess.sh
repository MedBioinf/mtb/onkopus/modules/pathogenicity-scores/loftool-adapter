
SOURCE_DIR="./data"
MODULE_DIR="loftool"

mkdir -v "${SOURCE_DIR}/${MODULE_DIR}"

FILENAME="bioinformatics_33_4_471_s1.zip"
echo "Downloading LoFTool database..."
wget -v "https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/bioinformatics/33/4/10.1093_bioinformatics_btv602/6/bioinformatics_33_4_471_s1.zip?Expires=1694638015&Signature=PDV-troFwMD3voLzaS~FHfOeZ4USXIvTqUEnhoXyB27RtTnubxfWC4PRlTqQzAFUqgJBrr2B52wKITAR0RnfeAJEl9FJaGfkBAlUEJB8ygPfa1ChYOKOtmxZ~-nRbqmDg~YgWEmHu7vvDJocnloFnu4wwDYib2tdDDgoc0B8DR2TDQkkacMYsGko10GE-iZWBPeV35SST2WZZf7sTuQhOo3eW6LIoJx2D~uqtQbxBVeDnEP5D8l3HR9odPMqBdsq1HhTw2ChB5sc2RrnSGzlA3PxTG2hRlxBoYLNLrubuIkYGn0KSPLnVi45DkVY-5nzt~EXB-keljC~C3rNMv70XA__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA" -O "${SOURCE_DIR}/${MODULE_DIR}/${FILENAME}"
cd ${SOURCE_DIR}/${MODULE_DIR}
unzip ${FILENAME}

/opt/venv/bin/python3 /app/app.py
