import traceback, re, subprocess, os
import conf.read_config as conf_reader
import pandas as pd


def get_entry(genes,variant_list, genome_version,data):
    """
    Returns a list of dictionaries containing the LoFTool scores and the associated genes

    :param genes:
    :param variant_list:
    :param genome_version:
    :param data:
    :return:
    """
    response = {}
    for i,gene in enumerate(genes):
        entry = data.loc[data["Gene"] == gene ]
        try:
            if "Gene" in entry:
                dc = {}
                dc["Gene"] = str(entry["Gene"].values.item())
                dc["Score"] = str(entry["ExACtool percentile"].values.item())
                dc["Synonymous"] = str(entry["Synonymous"].values.item())
                dc["hc_lof"] = str(entry["HC LoF"].values.item())
                dc["Top_expr_tissue"] = str(entry["top expr tissue (3x more)"].values.item())
                dc["office_of_rare_diseases"] = str(entry["office of rare diseases"].values.item())
                response[variant_list[i]] = {}
                response[variant_list[i]]["loftool"] = dc
        except:
            print("No LoFTool value found for gene ",gene)
            response[variant_list[i]] = {}
            response[variant_list[i]]["loftool"] = {}
    return response


def load_scores():
    """
    Loads the LoFTool scores data in a DataFrame

    :return:
    """
    __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
    db_file = __location__ + '/../data/' + conf_reader.__SCORE_FILE__
    data = pd.read_excel(db_file)
    return data

