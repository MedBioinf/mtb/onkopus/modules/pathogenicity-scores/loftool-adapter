# LoFtool Service

This microservice returns the LoFtool - Score of a gene. It currently uses the Excel file (*Table_S1*) provided by [this article](https://academic.oup.com/bioinformatics/article/33/4/471/2525582). 

It is a very early version (WIP).

More information about LoFtool can be found [here](https://academic.oup.com/bioinformatics/article/33/4/471/2525582).

The database files can be obtained [here](https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/bioinformatics/33/4/10.1093_bioinformatics_btv602/6/bioinformatics_33_4_471_s1.zip?Expires=1686042054&Signature=cLjHZ2CJ-5RfU7FpVHuR7h5~A10IkpBoN6MTQMxS3GwQsbIXl4~Mb7kSkuZ30Qi4lKnR0xEkTHeTnlYEUt4jBWllHsFvREw3JT0hC~7St4jrXqVW0G~I17lmYkA7DWHcx~0vLpmwBKnwef-4KS-5dKDPo4LvfwgOpQTvFAWITV2YQumOiCDs-M6HAGOqHvwarKjNLfVLX2gtb79QXQuKbcgqcGaQp60qLSSJ2cY6GgdX5rufm8LVfdmQhEmccX~JaF9f8QUBkW4zUtrIoVmxj2A8VT2bH~G9kPIxyQIEceBfvR-8n7aH29Fnw6mPufMF~RMLxq5sMXrtofaK8Vmcxg__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA). 

## Requirements <a name="requirements"></a>

If a docker-image is already provided, only [Docker](https://docs.docker.com/get-docker/) and docker-compose need to be installed. 

Otherwise [Maven](https://maven.apache.org/users/index.html) needs to be installed in addition to that.


## General Setup

1. Clone the git project
2. Install and set up the [Requirements](#requirements)

### If the docker image still needs to be build:

1. Run `mvnw package`, for example with:

```
./mvnw clean package
```


2. Create the docker image(or `tag` a container as `loftoolservice:latest`):

```
docker build -f src/main/docker/Dockerfile.jvm -t loftoolservice .
```

### With working docker images:

3. Run `docker-compose up`

4. Query the program with the wanted gene symbol (see below)


## Querying the service

The service accepts gene symbols in the HGNC format (https://www.genenames.org/) as query parameter. 

It will return some JSON in a preliminary format.

If the gene symbol is not found in the table, it will just return empty fields.

## Endpoints

Currently, there are two endpoints:

- `loftool/v1/full/`: Delivers JSON output of information provided by LoFtool for given genes.
- `loftool/v1/simple/`: Delivers JSON output of the score provided by LoFtool for given genes.
- `loftool/v1/info/`: Shows some basic information regarding the tool.

#### Example call (the service is currently running on port 8094):

```
curl -X GET "http://localhost:8094/loftool/v1/full?genesymbol=BRCA1,NIPBL,SCN5A" | jq
```

